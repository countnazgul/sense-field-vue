import Vue from 'vue'
import QlikField from './QlikField.vue'

Vue.config.productionTip = false

import 'leonardo-ui'
import 'leonardo-ui/dist/leonardo-ui.css'

new Vue({
  render: h => h(QlikField),
}).$mount('#qlikfield')
