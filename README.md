# Qlik Sense Field Component for VueJS

## Description

The purpose of this component is to mimic the QS selection field (current selections and selection tool) but to be used in Vue app

## Installation

First install the component

`npm install --save sense-field-vue`

And then start using it in your Vue app

    <template>
        <div id="app">
            <qlik-field/>
        </div>
    </template>

    <script>
        import QlikField from 'sense-field-vue'
        import QlikFieldStyles from 'sense-field-vue/dist/sense-field-vue.css'

        export default {
            name: 'app',
            components: {
                QlikField
            }
        }
    </script>

## Properties

The only mandatory property is `qData`. All other properties, if not supplied, will use the default values shown below 

   * qData - data returned from interaction with the Engine API. The data is the `qMatrix` value from the api call. Example:
 
    [
        [
          {
            qText: "This is text",
            qNum: 'NaN',
            qElemNumber: 0,
            qState: "S"
          }
        ],
        [
          {
            qText: "1",
            qNum: 1,
            qElemNumber: 1,
            qState: "O"
          }
        ],
        ...
    ]

   * clickAway - `true` or `false`. If `true` then clicking outside the field boundaries will "close" (hide) the component. Default is `true`   
   * showArrow - `true` or `false`. Show or hide the little arrow at the top
   * showSearch - `true` or `false`. Show or hide the search input
   * showHeader - `true` or `false`. Show or hide the whole header (the selection interactions)
   * bgColors - background colors for the field values. Defaults:

    {
        locked: "#3194f7",
        selected: "#52cc52",
        optional: "#f9f9f9",
        deselected: "#f9f9f9",
        alternative: "#ddddd",
        excluded: "#a9a9a9",
        excludedSelected: "#a9a9a9",
        excludedLocked: "#a9a9a9"
    }

   * fontColors - font colors for the field values. Defaults:

    {
        locked: "#ffffff",
        selected: "#ffffff",
        optional: "",
        deselected: "",
        alternative: "",
        excluded: "",
        excludedSelected: "",
        excludedLocked: ""
    }


   * headerColors - colors for the header section. DefaultS:

    {
        background: "#e6e6e6",
        icons: "#000"
    }

   * headerIcons - prop that control the visibility and enabled state of each icon in the header section. Defaults:
    
    {
        selectAll: {
            enabled: true,
            hidden: false
        },
        selectAlternative: {
            enabled: true,
            hidden: false
        },
        selectExcluded: {
            enabled: true,
            hidden: false
        },
        lock: {
            enabled: true,
            hidden: false
        },
        clearAll: {
            enabled: true,
            hidden: false
        }
    }



## Events

* clicked
* selectAll
* selectAlternative
* selectExcluded
* lock
* clearAll


## Screenshots

Default view

![Default](source/images/Screenshot1.png)

No header and arrow

![No header and arrow](source/images/Screenshot2.png)

Value colors change

![Value color change](source/images/Screenshot3.png)